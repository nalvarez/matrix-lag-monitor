#!/usr/bin/python3

# SPDX-FileCopyrightText: 2020 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import asyncio
import json
import os
import sys
import getpass

from nio import AsyncClient, LoginResponse

async def login(config_dict):
    homeserver = config_dict["homeserver"]
    user_id = config_dict["user_id"]

    client = AsyncClient(homeserver, user_id)
    pw = getpass.getpass(f"Password for user {user_id}: ")

    resp = await client.login(pw, device_name="chatlag.py")

    # check that we logged in succesfully
    if (isinstance(resp, LoginResponse)):
        await client.close()
        return {"device_id": resp.device_id, "access_token": resp.access_token}
    else:
        print(f"homeserver = \"{homeserver}\"; user = \"{user_id}\"")
        print(f"Failed to log in: {resp}")
        sys.exit(1)

def main():
    with open("setting.json", "r") as f:
        config = json.load(f)
    try:
        with open("secrets.json", "r") as f:
            secrets = json.load(f)
    except FileNotFoundError:
        secrets = {}

    client_id = sys.argv[1]
    if client_id not in config['matrix']:
        print(f"Couldn't find {client_id} in the settings", out=sys.stderr)
        sys.exit(1)

    resp = asyncio.get_event_loop().run_until_complete(login(config['matrix'][client_id]))

    secrets[client_id] = resp
    with open("secrets.json.new", "w") as f:
        json.dump(secrets, f, indent=4)

    os.rename("secrets.json.new", "secrets.json")

main()
