#!/usr/bin/python3

# SPDX-FileCopyrightText: 2020 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import asyncio
import re
import json
import time
import nio as matrix
import irc.client_aio
import aiohttp
import sys
import itertools

influxdb = None

class InfluxDB:
    def __init__(self, url, username, password, db):
        self.url = url
        self.db = db

        self.lines = []

        self.session = aiohttp.ClientSession(auth=aiohttp.BasicAuth(username, password))

    def add_line(self, line):
        """
        'line' is raw InfluxDB line protocol
        """
        if isinstance(line, str):
            line = line.encode('utf8')
        self.lines.append(line)

    async def send(self):
        if len(self.lines) == 0: return

        print("InfluxDB: flushing")
        for line in self.lines:
            print(line)
        async with self.session.post(
            self.url,
            params={'db': self.db, 'precision': 'ms'},
            data=b'\n'.join(self.lines)
        ) as r:
            if 200 <= r.status <= 299:
                print("Sent %d points to InfluxDB" % len(self.lines))
                self.lines = []

    async def send_loop(self, interval):
        await asyncio.sleep(10)
        self.lines = []
        while True:
            await asyncio.sleep(interval)
            await self.send()

class Manager:
    def __init__(self):
        self.clients = []

    def add_client(self, client):
        self.clients.append(client)

    def client_by_username(self, username):
        for client in self.clients:
            if client.user_id == username:
                return client
        else:
            return None

    def client_by_irc_nick(self, nick):
        for client in self.clients:
            if client.irc_nick == nick:
                return client
        else:
            return None

    def got_message(self, src_client, dest_id, sent_ts=0, server_ts=0, recv_ts=0):
        src_id = src_client.identifier
        print(f"resolved to client {src_id}")

        if server_ts:
            for tstype,ts_ms in (("sent", sent_ts), ("recv_server", server_ts), ("recv", recv_ts)):
                influxdb.add_line(f"chatlag,tstype={tstype},source={src_id},dest={dest_id} server_delay={server_ts-sent_ts},client_delay={recv_ts-sent_ts} {ts_ms}")
        else:
            for tstype,ts_ms in (("sent", sent_ts), ("recv", recv_ts)):
                influxdb.add_line(f"chatlag,tstype={tstype},source={src_id},dest={dest_id} client_delay={recv_ts-sent_ts} {ts_ms}")

    async def sync_all(self):
        await asyncio.gather(*(client.sync_forever() for client in self.clients))

    async def sendloop(self):
        await asyncio.sleep(10)
        for seq, client in enumerate(itertools.cycle(self.clients)):
            await client.send_message(seq)
            await asyncio.sleep(5)

class MatrixClient:
    def __init__(self, manager, identifier, config_dict):
        self.manager = manager

        self.client = matrix.AsyncClient(config_dict['homeserver'])
        self.client.access_token = config_dict['access_token']
        self.client.user_id      = config_dict['user_id']
        self.client.device_id    = config_dict['device_id']

        self.client.add_event_callback(self._message_callback, matrix.RoomMessageText)

        self.room_id     = config_dict['room_id']
        self.user_id     = config_dict['user_id']
        self.irc_nick    = config_dict['irc_nick']
        self.identifier  = identifier
        print(f"matrix client {identifier} connecting")

    async def _message_callback(self, room: matrix.MatrixRoom, event: matrix.RoomMessageText) -> None:
        recv_ms = int(time.time()*1000)
        recv_s = recv_ms/1000
        print("client %s got message %r" % (self.identifier, event))

        if room.room_id != self.room_id:
            # message is for another room
            return

        m = re.match('^MSGX SEQ:\d* TS:(\d+)$', event.body)
        if m:
            msg_ms = int(m.group(1))
            msg_s = msg_ms/1000
            server_ms = event.server_timestamp
            server_s = event.server_timestamp/1000
            print("%s server recv from %s in %.2fs" % (self.identifier, event.sender, server_s-msg_s))
            print("%s client recv from %s in %.2fs" % (self.identifier, event.sender, recv_s-msg_s))

            src_client = self.manager.client_by_username(event.sender)
            if not src_client:
                print("unknown client, ignoring")
                return

            self.manager.got_message(
                src_client = src_client,
                dest_id = self.identifier,
                sent_ts = msg_ms,
                server_ts = server_ms,
                recv_ts = recv_ms
            )

    async def send_message(self, seq):
        await self.client.room_send(self.room_id,
            message_type="m.room.message",
            content={"msgtype": "m.text", "body": "MSGX SEQ:%d TS:%d" % (seq, time.time()*1000)}
        )

    async def sync_forever(self):
        await self.client.sync_forever(timeout=30000)

class IRCBot(irc.client_aio.AioSimpleIRCClient):
    def __init__(self, manager):
        super().__init__()
        self.manager = manager
        self.identifier = "irc"

    @property
    def irc_nick(self):
        return self.connection.real_nickname

    @property
    def user_id(self):
        return f"@_freenode_{self.irc_nick}:kde.org"

    def on_welcome(self, conn, event):
        print(event)
        conn.join("#kde-lagtest")

    def on_join(self, conn, event):
        print(event)
        if event.source.nick == self.connection.real_nickname:
            print("We just joined")

    def on_pubmsg(self, conn, event):
        recv_ms = int(time.time()*1000)
        recv_s = recv_ms/1000
        print(event)
        if event.target == "#kde-lagtest":
            msg = event.arguments[0]
            m = re.match('^MSGX SEQ:\d* TS:(\d+)$', msg)
            if m:
                msg_ms = int(m.group(1))
                msg_s = msg_ms/1000
                print("IRC client got message from %s sent %.2fs ago" % (event.source.nick, recv_s-msg_s))

                src_client = self.manager.client_by_irc_nick(event.source.nick)
                if not src_client:
                    print("unknown client, ignoring")
                    return

                self.manager.got_message(
                    src_client = src_client,
                    dest_id = self.identifier,
                    sent_ts = msg_ms,
                    recv_ts = recv_ms
                )

    async def send_message(self, seq):
        self.connection.privmsg("#kde-lagtest", "MSGX SEQ:%d TS:%d" % (seq, time.time()*1000))

    async def sync_forever(self):
        pass


async def main() -> None:
    with open("setting.json", "r") as f:
        config = json.load(f)
    with open("secrets.json", "r") as f:
        secrets = json.load(f)

    global influxdb
    influxdb = InfluxDB("http://overwatch.kde.org:8086/write", config["influxdb"]["username"], secrets["influxdb_pw"], "telegraf")

    mgr = Manager()

    for identifier, config_dict in config['matrix'].items():
        config_dict["access_token"] = secrets[identifier]["access_token"]
        config_dict["device_id"] = secrets[identifier]["device_id"]
        client = MatrixClient(mgr, identifier, config_dict)
        mgr.add_client(client)

    iclient = IRCBot(mgr)

    print("connecting to IRC")
    await iclient.connection.connect(config['irc']['server'], 6667, config['irc']['nickname'])
    mgr.add_client(iclient)
    print("done")

    asyncio.ensure_future(mgr.sendloop())
    asyncio.ensure_future(influxdb.send_loop(5))

    await mgr.sync_all()

asyncio.get_event_loop().run_until_complete(main())
